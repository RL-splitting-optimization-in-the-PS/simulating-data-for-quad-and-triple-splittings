import argparse
import pickle
from datetime import datetime
from time import perf_counter
from run_quad_sim import objective_function, simulate_bunch_lengths_ms
import numpy as np

DATA_PATH = "/eos/home-j/jwulff/SWAN_projects/batch_jobs2/outputs/datamatrix_files/quad/machine_settings_fixed/"


def run(phase_42, phase_84, meta):
    # before run
    meta['simulation_setup_iso'] = datetime.now().astimezone().replace(microsecond=0).isoformat()
    meta['simulation_setup_utc'] = int(round(datetime.timestamp(datetime.utcnow())))
    meta['simulation_setup_perf'] = perf_counter()
    out_s = 'Simulation setup start date: %s\n' % meta['simulation_setup_iso']
    out_s += 'Simulation setup start unix timestamp UTC: %s\n' % meta['simulation_setup_utc']
    out_s += 'Simulation setup start perf: %f\n' % meta['simulation_setup_perf']
    print(out_s)
    # to measure the time it takes to set up the simulation
    meta['simulation_run_start_perf'] = perf_counter()
    # run your main code
    print("starting simulation...")
    output = simulate_bunch_lengths_ms([phase_42, phase_84])
    print("Finished simulation...")
    meta['simulation_run_finish_perf'] = perf_counter()
    # save your results
    if output != False:
        print("output not false, saving")
        result = np.array([phase_42, phase_84, output[1]], dtype = object) # Save inputs and outputs
        datamatrix = output[0]
        profile = output[2]
        np.save(DATA_PATH + "profiles/p42_{}_p84_{}_profile".format(int(phase_42*100), int(phase_84*100)), profile)
        np.save(DATA_PATH + "blengths/p42_{}_p84_{}_blengths".format(int(phase_42*100), int(phase_84*100)), result)
        np.save(DATA_PATH + "p42_{}_p84_{}_datamatrix".format(int(phase_42*100), int(phase_84*100)), datamatrix)
        #print("Hopefully saved to /eos/home-j/jwulff/SWAN_projects/batch_jobs2/outputs/datamatrix_files/quad/p42_{}_p84_{}_datamatrix".format(int(phase_42*100), int(phase_84*100)))
    # measure the time it takes to save the run output files and save this into a meta file
    meta['simulation_save_finish_perf'] = perf_counter()
    meta['simulation_save_finish_iso'] = datetime.now().astimezone().replace(microsecond=0).isoformat()
    meta['simulation_save_finish_unix'] = int(round(datetime.timestamp(datetime.utcnow())))
    times_filename = "/afs/cern.ch/work/j/jwulff/test/meta/meta_p42_{}_p84_{}.pkl".format(phase_42, phase_84)
    with open(times_filename, 'wb') as pf:
        pickle.dump(meta, pf)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p42', '--phase_42', type=float)
    parser.add_argument('-p84', '--phase_84', type=float)
    parser.add_argument('-i', '--submit_iso', type=str)
    parser.add_argument('-t', '--submit_unix', type=int)
    parser.add_argument('-bsd', '--run_bash_iso', type=str)
    parser.add_argument('-bsutc', '--run_bash_unix', type=int)
    parser.add_argument('-scalld', '--simulation_call_iso', type=str)
    parser.add_argument('-scallutc', '--simulation_call_unix', type=int)
    parser.add_argument('-cl', '--cluster', type=str)
    parser.add_argument('-proc', '--process', type=str)
 
    args = parser.parse_args()
    meta = {'submit_iso': args.submit_iso, 'submit_unix': args.submit_unix,
             'run_bash_iso': args.run_bash_iso, 'run_bash_unix': args.run_bash_unix,
             'simulation_call_iso': args.simulation_call_iso, 'simulation_call_unix': args.simulation_call_unix,
            'cluster': args.cluster, 'job_id': args.process}
    sm = 'Submission time\n'
    sm += 'Submission date: %s\n' % args.submit_iso
    sm += 'Submission unix timestamp UTC: %d\n' % args.submit_unix
    print(sm)
    run(args.phase_42, args.phase_84, meta)
