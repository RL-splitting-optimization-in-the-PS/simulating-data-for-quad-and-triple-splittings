import argparse
import pickle
from datetime import datetime
from time import perf_counter
from run_trisplit_sim import simulate_bunch_lengths_MS, simulate_bunch_lengths, simulate_bunch_lengths_w_int
import os
import numpy as np

#DATA_PATH = "/eos/home-j/jwulff/SWAN_projects/batch_jobs2/outputs/datamatrix_files/tri/machine_settings_fixed/"
#DATA_PATH = "/eos/home-j/jwulff/SWAN_projects/batch_jobs2/outputs/datamatrix_files/tri/ref_h14_refvf998/"
DATA_PATH = "/eos/home-j/jwulff/SWAN_projects/batch_jobs2/outputs/datamatrix_files/tri/init_type_1/"

def run(phase_14, phase_21, v14_factor, meta, init_distribution = 0):
    # before run
    meta['simulation_setup_iso'] = datetime.now().astimezone().replace(microsecond=0).isoformat()
    meta['simulation_setup_utc'] = int(round(datetime.timestamp(datetime.utcnow())))
    meta['simulation_setup_perf'] = perf_counter()
    out_s = 'Simulation setup start date: %s\n' % meta['simulation_setup_iso']
    out_s += 'Simulation setup start unix timestamp UTC: %s\n' % meta['simulation_setup_utc']
    out_s += 'Simulation setup start perf: %f\n' % meta['simulation_setup_perf']
    print(out_s)
    # to measure the time it takes to set up the simulation
    meta['simulation_run_start_perf'] = perf_counter()
    # run your main code
    print('run_before')
    output = simulate_bunch_lengths_MS([phase_14, phase_21], v14_factor, init_distribution)
    print('bl ', output)
    meta['simulation_run_finish_perf'] = perf_counter()
    # save your results
    #if output[0] != None:
    result = np.array([phase_14, phase_21, v14_factor, output[1]], dtype = object) # Save inputs and outputs
    print('results ', result)
    datamatrix = output[0]
    profile = output[2]
    #np.save(DATA_PATH + "profiles/p14_{}_p21_{}_v14_{}_profile".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000)), profile)
    #np.save(DATA_PATH + "blengths/p14_{}_p21_{}_v14_{}_blengths".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000)), result)
    np.save(DATA_PATH + "p14_{}_p21_{}_v14_{}_datamatrix".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000)), datamatrix)
    # save your results 
    # measure the time it takes to save the run output files and save this into a meta file
    meta['simulation_save_finish_perf'] = perf_counter()
    meta['simulation_save_finish_iso'] = datetime.now().astimezone().replace(microsecond=0).isoformat()
    meta['simulation_save_finish_unix'] = int(round(datetime.timestamp(datetime.utcnow())))
    times_filename = "/afs/cern.ch/work/j/jwulff/testbatch/meta/meta_p14_{}_p42_{}_v14_{}.pkl".format(phase_14*100, phase_21*100, v14_factor*1000)
    # with open(times_filename, 'wb') as pf:
    #     pickle.dump(meta, pf)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p14', '--phase_14', type=float)
    parser.add_argument('-p21', '--phase_21', type=float)
    parser.add_argument('-v14', '--v14_factor', type=float)
    parser.add_argument('-id', '--init_distribution', type=int)
    parser.add_argument('-i', '--submit_iso', type=str)
    parser.add_argument('-t', '--submit_unix', type=int)
    parser.add_argument('-bsd', '--run_bash_iso', type=str)
    parser.add_argument('-bsutc', '--run_bash_unix', type=int)
    parser.add_argument('-scalld', '--simulation_call_iso', type=str)
    parser.add_argument('-scallutc', '--simulation_call_unix', type=int)
    parser.add_argument('-cl', '--cluster', type=str)
    parser.add_argument('-proc', '--process', type=str)

    args = parser.parse_args()
    meta = {'submit_iso': args.submit_iso, 'submit_unix': args.submit_unix,
             'run_bash_iso': args.run_bash_iso, 'run_bash_unix': args.run_bash_unix,
             'simulation_call_iso': args.simulation_call_iso, 'simulation_call_unix': args.simulation_call_unix,
            'cluster': args.cluster, 'job_id': args.process}
    sm = 'Submission time\n'
    sm += 'Submission date: %s\n' % args.submit_iso
    sm += 'Submission unix timestamp UTC: %d\n' % args.submit_unix
    print(sm)
    print('run main')
    if args.init_distribution == None:
        run(args.phase_14, args.phase_21, args.v14_factor, meta, init_distribution=1) #CHANGE TO TAKE AWAY INIT LATER!!
    else:
        run(args.phase_14, args.phase_21, args.v14_factor, meta, init_distribution=1)# args.init_distribution)
