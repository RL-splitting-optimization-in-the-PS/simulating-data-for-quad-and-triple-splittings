import glob
import numpy as np
import os
import csv

#DATA_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\quad\machine_settings_fixed"
SAVE_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\missing_files_quad.csv"    
DATA_PATH = r"\\eosproject-smb\eos\project\s\sy-rf-br\Machines\PS\DatasetsForML\Quadsplit"

p1s = np.linspace(-30,30,121)
p2s = np.linspace(-30,30,121)
list_of_filenames = []
for p1 in p1s:
    for p2 in p2s:
        list_of_filenames.append("p42_{}_p84_{}_datamatrix.npy".format(int(p1*100), int(p2*100)))
filenames = []
missing_files = []
for file in glob.glob(DATA_PATH + '/*.npy'): # Stopped working for some reason ???
    
    filename = os.path.basename(file)
    filenames.append(filename)

missing_files = list((set(list_of_filenames) - set(filenames)))

if len(missing_files) > 0:
    settings_csv_list = []
    for entry in missing_files:
        split = entry.split('_')
        p14 = (int(split[1])/100)
        p21 = (int(split[3])/100)
        settings_csv_list.append([p14,p21])
    with open(SAVE_PATH, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(settings_csv_list)
print('Done creating csv.')
print('Check complete. {} missing_files.'.format(len(missing_files)))
if len(missing_files) < 11:
    print(missing_files)