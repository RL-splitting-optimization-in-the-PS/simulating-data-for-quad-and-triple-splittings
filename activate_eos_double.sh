#!/bin/bash
SIM=/afs/cern.ch/work/j/jwulff/testbatch
HOME=/afs/cern.ch/work/j/jwulff
echo "Bash started"
bsd=`date --iso-8601="seconds"`
bsutc=`date --utc +%s`
echo "Bash start date: "$bsd
echo "Bash start unix timestamp UTC: "$bsutc
source $HOME/blond_venv/bin/activate
echo "Virtual environment activated"
scalld=`date --iso-8601="seconds"`
scallutc=`date --utc +%s`
echo "Simulation call date: "$scalld
echo "Simulation call unix timestamp UTC: "$scallutc
mkdir eos
echo "Directory contents before simulation:
"echo `ls -al`
python $SIM/run_eos_double.py "$@" -bsd $bsd -bsutc $bsutc -scalld $scalld -scallutc $scallutc
echo "Directory contents after simulation:"
echo `ls -al`
echo "Run finished
"echo "Simulation end date: "`date --iso-8601="seconds"`
echo "Simulation unix timestamp UTC: "`date --utc +%s`
