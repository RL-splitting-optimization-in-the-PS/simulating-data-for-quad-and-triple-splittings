# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:31:52 2021

@author: Joel Wulff
"""

# Example input for HTcondor:  python3 submit.py -p10 0 -p11 0 -p1p 1 -p20 0 -p21 0 -p2p 1

from cProfile import run
from cgitb import small
#from scipy import fftpack
import numpy as np
#import imageio
import matplotlib.pyplot as plt
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate as interp

#from simulation import Simulation
from simulation_doublesplit import Simulation_MS

def simulate_bunch_lengths_ms(initial_phase_errors): 

    sim = Simulation_MS(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.get_datamatrix()


if __name__ == "__main__":
    start = perf_counter()
    #run_sim([0,0])
    #res = objective_function([0,0])
    #print('Result {}'.format(res))
    #test_one_run()
    #test_optimizer_lookup()
    results = simulate_bunch_lengths_ms([0,0])
    plt.figure()
   
    stop = perf_counter()
    print('The script took %f s to run.' % (stop-start))