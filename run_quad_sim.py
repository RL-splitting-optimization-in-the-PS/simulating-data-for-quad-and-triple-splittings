# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:31:52 2021

@author: Joel Wulff
"""
import numpy as np
import scipy.optimize as opt
import os
from scipy import interpolate as interp
from time import perf_counter


from simulation import Simulation
from simulation_machine_settings import Simulation_MS

def objective_function(initial_phase_errors, lxplus = False, splitting='double'):
    if lxplus:
        for file in os.listdir("/afs/cern.ch/user/j/jwulff/test/eos/batch_jobs2/outputs/npy_files"):
            if file == "p42_{}_p84_{}.npy".format(int(100*initial_phase_errors[0]), int(100*initial_phase_errors[1])):
                print("Already have .npy file with name {}, skipping...".format(file))
                return False
    sim = Simulation(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.process_data()


def simulate_bunch_lengths(initial_phase_errors,  lxplus = False):
    
    #if lxplus:
    #    for file in os.listdir("/afs/cern.ch/user/j/jwulff/testbatch/eos/outputs/datamatrix_files/quad/corrected"):
    #        if file == "p42_{}_p84_{}_blengths.npy".format(int(100*initial_phase_errors[0]), int(100*initial_phase_errors[1])):
    #            print("Already have .npy file with name {}, skipping...".format(file))
    #            return False
    sim = Simulation(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.get_datamatrix()

def simulate_bunch_lengths_ms(initial_phase_errors,  lxplus = False):
    
    #if lxplus:
    #    for file in os.listdir("/afs/cern.ch/user/j/jwulff/testbatch/eos/outputs/datamatrix_files/quad/corrected"):
    #        if file == "p42_{}_p84_{}_blengths.npy".format(int(100*initial_phase_errors[0]), int(100*initial_phase_errors[1])):
    #            print("Already have .npy file with name {}, skipping...".format(file))
    #            return False
    sim = Simulation_MS(initial_phase_errors = initial_phase_errors)
    sim.run_sim()
    return sim.get_datamatrix()

def callbackF(xk):
    global Neval
    print("Iterations: {}  Current pos {}".format(Neval, xk))
    Neval += 1
    return

def optimization_lookup(initial_phase_errors, lookup_table, scaling=False):
    rows, cols = np.shape(lookup_table)
    spline = interp.RectBivariateSpline(np.arange(0,rows), 
                                        np.arange(0,cols), 
                                        lookup_table)
    # print(lookup_table[5,5])
    # print(lookup_table[5,6])
    # test = spline.__call__(5.5, 5.6)
    
    # Translate phase errors to indexes
    p42 = initial_phase_errors[0] + 45
    p84 = initial_phase_errors[1] + 45
    #print("p42 idx {}, and p84 idx {}".format(p42, p84))
    result = spline.__call__(p42, p84)[0][0]
    #print(result)
    return result
    
    
def test_optimizer_lookup():
    lookup_table = np.load('lookup_table_0.1.npy')
    initial_phase_errors = [5, -5]
    res = opt.minimize(optimization_lookup, initial_phase_errors, args=(lookup_table), method = 'Nelder-Mead', callback=callbackF)

Neval = 1


def testfunction(x):
    return x**2

def test_optimizer(opt_param = 'length'):
    if opt_param == 'length':
        x0 = 10 # Start position
        res = opt.minimize(testfunction, x0, method='Nelder-Mead', callback=callbackF)
        print("Success: {}, Solution {}, Nbr of iterations {}".format(res.success, res.x, res.nit))
    return res

def optimizer(opt_param = 'length'):
    if opt_param == 'length':
        x0 = np.array([10, -10]) # Start position
        res = opt.minimize(objective_function, x0, method='Nelder-Mead', callback=callbackF)
        print("Success: {}, Solution {}, Nbr of iterations {}".format(res.success, res.x, res.nit))
    return None

  # Value of sum of np.abs(fft(bunchLengths))
                                # for the two peaks in freq. spectrum
def f(x):
    return objective_function(x)

def create_new_lookup_table():
    H84_range = range(-10,10)
    H42_range = range(-10,10)
    table = np.zeros((abs(H42_range.stop-H42_range.start)+1,
                      abs(H84_range.stop-H84_range.start)+1))
    for i, phase_42 in enumerate(H42_range):
        for j, phase_84 in enumerate(H84_range):
            sum_mod_error = objective_function([phase_42, phase_84])
            table[i][j] = sum_mod_error
    
    
def obj_function_using_lookup(phase_42, phase_84):
    lookup_table = np.load('lookup_table_0.1.npy')
    

def test_one_run():
    output = objective_function([0, 0])
    print(output)

    

def testing():
    H84_range = range(-10,11)
    H42_range = range(-10,11)
    input_phases_84 = np.empty(0)
    input_phases_42  = np.empty(0)
    table = np.zeros((abs(H42_range.stop-H42_range.start),
                      abs(H84_range.stop-H84_range.start)))
    for i, phase_42 in enumerate(H42_range):
        input_phases_42 = np.append(input_phases_42, phase_42)
        for j, phase_84 in enumerate(H84_range):
            print("Phase 42 nbr: {}/{}, phase 84 nbr {}/{}".format(i, len(H42_range),
                                                                   j, len(H84_range)))
            sum_mod_error = objective_function([phase_42, phase_84])
            table[i][j] = sum_mod_error
            input_phases_84 = np.append(input_phases_84, phase_84)
           
    np.save('input_phases_84', input_phases_84)
    np.save('input_phases_42', input_phases_42)
    np.save('lookup_table_3', table)
    

def run_sim():
    sim = Simulation_MS()
    phase_offsets = optimizer(sim.run_sim())
    

if __name__ == "__main__":
    start = perf_counter()
    results = simulate_bunch_lengths_ms([0.,0.])
    #res = objective_function([0,0])
    #print('Result {}'.format(res))
    #test_one_run()
    #test_optimizer_lookup()
    #datamatrix, profile, rf_station = simulate_bunch_lengths([0,0])
    #plt.figure()
    #plt.imshow(datamatrix, aspect='auto', origin='lower')
    #plt.xlim((0, 1000))

    #plt.figure()
    #plt.plot(profile.bin_centers, datamatrix[400,:])
    #plt.xlim((0, rf_station.t_rf[0,0]))
    stop = perf_counter()
    print('The script took %f s to run.' % (stop-start))