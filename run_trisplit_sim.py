# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 08:31:52 2021

@author: Joel Wulff
"""
#from scipy import fftpack
import numpy as np
import scipy.optimize as opt
from time import perf_counter
from scipy import interpolate as interp
import matplotlib.pyplot as plt

#from trisplit_simulation import TrisplitSimulation
from trisplit_simulation_machine_settings import TrisplitSimulationMS

def simulate_bunch_lengths(initial_phase_errors, v14_factor, lxplus = False):
    
    sim = TrisplitSimulation(initial_phase_errors = initial_phase_errors, v14_factor=v14_factor)
    print('hello')
    sim.run_sim()
    return sim.get_datamatrix() # FIX get datamatrix

def simulate_bunch_lengths_MS(initial_phase_errors, v14_factor, init_distribution):
    
    sim = TrisplitSimulationMS(initial_phase_errors = initial_phase_errors, v14_factor=v14_factor, init_distribution=init_distribution)
    sim.run_sim()
    return sim.get_datamatrix()

def simulate_bunch_lengths_w_int(initial_phase_errors, v14_factor, lxplus = False):
    
    sim = TrisplitSimulation(initial_phase_errors = initial_phase_errors, v14_factor=v14_factor)
    print('hello')
    sim.run_sim()
    return sim.bl_bi()

def run_batch_of_simulations(list_of_settings, lxplus = False):
    results = []
    for settings in list_of_settings:
        initial_phase_errors = settings[0:2]
        v14_factor = settings[2]
        sim = TrisplitSimulation(initial_phase_errors = initial_phase_errors, v14_factor=v14_factor) # Init sim
        sim.run_sim() # Runs sim
        result = sim.get_datamatrix() # Results are array of datamatrix, BLs, profiles
        results.append(result)
    return results


def run_sim():
    sim = TrisplitSimulation(initial_phase_errors = [0,0], v14_factor = 1.)
    sim.run_sim()
    

if __name__ == "__main__":
    #Used for testing
    start = perf_counter()
    outputs1 = simulate_bunch_lengths_MS(initial_phase_errors=[0,0], v14_factor=1.0, init_distribution=1)
    #outputs2 = simulate_bunch_lengths_MS(initial_phase_errors=[0,0], v14_factor=0.989)
    #outputs3 = simulate_bunch_lengths_MS(initial_phase_errors=[0,0], v14_factor=0.991)
    #outputs4 = simulate_bunch_lengths_MS(initial_phase_errors=[0,0], v14_factor=0.98)
    
    plt.figure()
    #plt.subplot(221)
    plt.title('v14: 1.0')
    plt.imshow(outputs1[0])
    # plt.subplot(222)
    # plt.title('v14: 0.989')
    # plt.imshow(outputs2[0])
    # plt.subplot(223)
    # plt.title('v14: 0.991')
    # plt.imshow(outputs3[0])
    # plt.subplot(224)
    # plt.title('v14: 0.98')
    # plt.imshow(outputs4[0])

    plt.figure()
    plt.subplot(221)
    plt.title('v14: 1.0')
    plt.plot(outputs1[0][-1,:])
    plt.subplot(222)
    plt.title('v14: 0.989')
    plt.plot(outputs2[0][-1,:])
    plt.subplot(223)
    plt.title('v14: 0.991')
    plt.plot(outputs3[0][-1,:])
    plt.subplot(224)
    plt.title('v14: 0.98')
    plt.plot(outputs4[0][-1,:])
    #print(blbi)
    #res = objective_function([0,0])
    #print('Result {}'.format(res))
    #test_one_run()
    #test_optimizer_lookup()
