import argparse
import subprocess
import os
import numpy as np
from datetime import datetime
 
# Example input:  python3 submit_eos_quad.py -p10 0 -p11 0 -p1p 1 -p20 0 -p21 0 -p2p 1


def write_file(phase_42, phase84):
    s = "executable = activate_eos.sh\n"
    local_iso_ts = datetime.now().astimezone().replace(microsecond=0).isoformat()
    utc_unix_ts = int(round(datetime.timestamp(datetime.utcnow())))
    s += "arguments = -p42 %f -p84 %f -i %s -t %d -cl $(Cluster) -proc $(Process) \n" %\
        (phase_42, phase84, local_iso_ts, utc_unix_ts)
    #s += "log = outputs/quad/log_p42{}p84{}.log\n".format(int(phase_42*100), int(phase_84*100))
    s += "error = outputs/quad/test.err\n"#.format(int(phase_42*100), int(phase_84*100))
    #s += "output = outputs/quad/p42{}p84{}.out\n".format(int(phase_42*100), int(phase_84*100))
    s += "request_cpus = 1\n"
    s += "request_disk = 25 MB\n"
    s += "request_memory = 512 MB\n"
    s += "transfer_output_files = eos\n"
    s += "when_to_transfer_output = ON_EXIT\n"
    s += "+MaxDuration = 720\n"
    s += "queue\n"
    return s

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p10', '--start_phase_1', type=float)
    parser.add_argument('-p11', '--stop_phase_1', type=float)
    parser.add_argument('-p1p', '--phase_1_points', type=int)
    parser.add_argument('-p20', '--start_phase_2', type=float)
    parser.add_argument('-p21', '--stop_phase_2', type=float)
    parser.add_argument('-p2p', '--phase_2_points', type=int)

    args = parser.parse_args()
    phase_42s = np.linspace(args.start_phase_1, args.stop_phase_1, args.phase_1_points)
    phase_84s = np.linspace(args.start_phase_2, args.stop_phase_2, args.phase_2_points)

    for phase_42 in phase_42s:
        for phase_84 in phase_84s:
            f = write_file(phase_42, phase_84)
            sub = "subs/p42{}p84{}.sub".format(int(phase_42*100),int(phase_84*100))
            with open(sub, 'w') as sf:
                sf.writelines(f)
            subprocess.run(['condor_submit', '%s' % sub])
