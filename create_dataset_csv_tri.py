# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:44:50 2021

@author: Joel Wulff
"""

# -*- coding: utf-8 -*-
import sys
# sys.path.append(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl') # Old solution
from pathlib import Path
mod_path = Path(__file__).parent # Add base of git repo to path.
sys.path.append(mod_path)
import numpy as np
import os
import matplotlib.pyplot as plt
import csv
import glob

DATA_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\total_tri_live_data"#r"C:\Users\jwulff\cernbox\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\init_type_1\volt_scan" # ref_h14_refvf998"
#DATA_PATH = r"H:\user\j\jwulff\\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\machine_settings_fixed"
#DATA_PATH = r"\\eoshome-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\ref_h14_refvf998"
SAVE_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\total_tri_live_data\live_tri_data.csv"
#DATA_PATH = "//eosproject-smb/eos/project/s/sy-rf-br/Machines/PS/DatasetsForML/Trisplit"
#loaded_values = []
first_file = True

filepaths = []
dictionary = None # key 1 is p14*100, key 2 is p21 * 100, key 3 is v14 * 100, value is filepath
p14_labels = None
p21_labels = None
v14_labels = None
first_file=True

create_custom_csv = False
if create_custom_csv:
    p14s = np.linspace(-20,20,41)*100
    p21s = np.linspace(-20,20,41)*100
    v14s = np.linspace(0.95,1.05,21)*1000

    #print(file_path)

    for p14 in p14s:
        for p21 in p21s:
            for v14 in v14s:
                p14 = int(p14)
                p21 = int(p21)
                v14 = int(round(v14))
                file = DATA_PATH + r"\p14_{}_p21_{}_v14_{}_datamatrix.npy".format(p14, p21, v14)
                if first_file:
                    labels_filepaths = np.array([p14, p21, v14, file], dtype=object)# phase 14, phase 21, voltage 14, datamatrix
                    first_file = False
                else:    
                    #loaded = np.load(file_path, allow_pickle=True)
                    labels_filepaths = np.vstack((labels_filepaths, np.array([p14, p21, v14, file], dtype=object)))

else:


    for file in glob.glob(DATA_PATH + '/*.npy'):
        filename = os.path.basename(file)
        filepaths.append(file)


        #print(file_path)

        # Extract labels from filepath
        split = filename.split('_')
        p14 = int(split[1])
        p21 = int(split[3])
        v14 = int(float(split[5]))
        if first_file:
            labels_filepaths = np.array([p14, p21, v14, file], dtype=object)# phase 14, phase 21, voltage 14, datamatrix
            first_file = False
        else:    
            #loaded = np.load(file_path, allow_pickle=True)
            labels_filepaths = np.vstack((labels_filepaths, np.array([p14, p21, v14, file], dtype=object)))
with open(SAVE_PATH, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(labels_filepaths)
print('Done creating csv.')