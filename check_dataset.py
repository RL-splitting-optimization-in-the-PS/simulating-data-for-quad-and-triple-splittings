import glob
import numpy as np
import os
import csv
DATA_PATH = r"\\eoshome-smb\eos\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\init_type_1"
#DATA_PATH = r"H:\user\j\jwulff\SWAN_projects\batch_jobs2\outputs\datamatrix_files\tri\machine_settings_fixed"
SAVE_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\missing_files.csv"    
#DATA_PATH = r"\\eosproject-smb\eos\project\s\sy-rf-br\Machines\PS\DatasetsForML\trisplit_test\machine_settings_fixed"
p1s = np.linspace(-20,20,41)
p2s = np.linspace(-20,20,41)
v14s = np.linspace(0.95,1.05,21)
list_of_filenames = []
for p1 in p1s:
    for p2 in p2s:
        for v14 in v14s:
            list_of_filenames.append("p14_{}_p21_{}_v14_{}_datamatrix.npy".format(int(p1*100), int(p2*100), int(round(v14*1000))))
filenames = []
missing_files = []
for file in glob.glob(DATA_PATH + r'\*.npy'):
    
    filename = os.path.basename(file)
    filenames.append(filename)
for entry in filenames:
    split = entry.split('_')
    p14 = (int(split[1])/100)
    p21 = (int(split[3])/100)
    v14 = (int(split[5])/1000)
    # #Rename if needed
    # if v14 == 1.014:
    #     split[5] = '1015'
    #     os.rename(DATA_PATH + '\\' + entry, DATA_PATH + '\\' +  '_'.join(split))

missing_files = list((set(list_of_filenames) - set(filenames)))



if len(missing_files) > 0:
    settings_csv_list = []
    for entry in missing_files:
        split = entry.split('_')
        p14 = (int(split[1])/100)
        p21 = (int(split[3])/100)
        v14 = (int(split[5])/1000)

        
        settings_csv_list.append([p14,p21,v14])
    with open(SAVE_PATH, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(settings_csv_list)
print('Done creating csv.')
print('Check complete. {} missing_files.'.format(len(missing_files)))
if len(missing_files) < 11:
    print(missing_files)