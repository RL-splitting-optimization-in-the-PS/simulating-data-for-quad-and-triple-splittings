import argparse
import subprocess
import os
import numpy as np
from datetime import datetime
import csv

# Example input:  python3 submit_eos_tri.py -p10 0 -p11 0 -p1p 1 -p20 0 -p21 0 -p2p 1 -v140 1 -v141 1 -v14p 1
 
def write_file(phase_14, phase_21, v14_factor):
    s = "executable = activate_eos_tri.sh\n"
    local_iso_ts = datetime.now().astimezone().replace(microsecond=0).isoformat()
    utc_unix_ts = int(round(datetime.timestamp(datetime.utcnow())))
    s += "arguments = -p14 %f -p21 %f -v14 %f -i %s -t %d -cl $(Cluster) -proc $(Process) \n" %\
        (phase_14, phase_21, v14_factor, local_iso_ts, utc_unix_ts)
    #s += "log = outputs/tri/log_p14{}p21{}v14{}.log\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    #s += "error = outputs/test.err\n" #tri/p14{}p21{}v14{}.err\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    #s += "output = outputs/tri/p14{}p21{}v14{}.out\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    s += "request_cpus = 1\n"
    s += "request_disk = 25 MB\n"
    s += "request_memory = 256 MB\n"
    s += "transfer_output_files = eos\n"
    s += "when_to_transfer_output = ON_EXIT\n"
    s += "+MaxDuration = 900\n"
    s += "queue\n"
    return s

if __name__ == '__main__':
    #parser = argparse.ArgumentParser()
    #parser.add_argument('-csv', '--path_to_csv_file', type=str)

    #args = parser.parse_args()

    csv_file = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/missing_files.csv" #_to_collect.csv"#args.path_to_csv_file
    with open(csv_file, newline='') as f:
            reader = csv.reader(f)
            settings_to_run = np.array(list(reader))
    #phase_14s = settings_to_run[:,0]
    #phase_21s = settings_to_run[:,1]
    #v14_factors = settings_to_run[:,2]
 
    for setting in settings_to_run:
        phase_14 = np.float(setting[0])
        phase_21 = np.float(setting[1])
        v14_factor = np.float(setting[2])
        f = write_file(phase_14, phase_21, v14_factor)
        sub = "subs/p14{}p21{}v14{}.sub".format(int(phase_14*100),int(phase_21*100), int(v14_factor*1000))
        with open(sub, 'w') as sf:
            sf.writelines(f)
        subprocess.run(['condor_submit', '%s' % sub])
