# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:44:50 2021

@author: Joel Wulff
"""

# -*- coding: utf-8 -*-
import sys
# sys.path.append(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl') # Old solution
from pathlib import Path
mod_path = Path(__file__).parent # Add base of git repo to path.
sys.path.append(mod_path)
import numpy as np
import os
import matplotlib.pyplot as plt
import csv
import glob

#SAVE_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\machine_settings_fixed\dataset.csv"
SAVE_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\all_live_data\dataset.csv"
#DATA_PATH = r"H:\user\j\jwulff\\SWAN_projects\batch_jobs2\outputs\datamatrix_files\quad\machine_settings_fixed"
DATA_PATH = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\all_live_data"
# SAVE_PATH = "dataset/quad/machine_settings_fixed/dataset.csv"
# #DATA_PATH = r"H:\user\j\jwulff\\SWAN_projects\batch_jobs2\outputs\datamatrix_files\quad\machine_settings_fixed\profiles"
# DATA_PATH = "//eosproject-smb/eos/project/s/sy-rf-br/Machines/PS/DatasetsForML/Quadsplit"
#loaded_values = []
first_file = True

filepaths = []
dictionary = None # key 1 is p42*100, key 2 is p84 * 100, value is filepath
p42_labels = None
p84_labels = None
first_file=True
for file in glob.glob(DATA_PATH + '/*.npy'):
    filename = os.path.basename(file)
    filepaths.append(file)


    #print(file_path)

    # Extract labels from filepath
    split = filename.split('_')
    p42 = int(split[1])
    p84 = int(split[3])
    if first_file:
        labels_filepaths = np.array([p42, p84, file], dtype=object)# phase 42, phase 84, datamatrix
        first_file = False
    else:    
        #loaded = np.load(file_path, allow_pickle=True)
        labels_filepaths = np.vstack((labels_filepaths, np.array([p42, p84, file], dtype=object)))
with open(SAVE_PATH, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(labels_filepaths)
print('Done creating csv.')