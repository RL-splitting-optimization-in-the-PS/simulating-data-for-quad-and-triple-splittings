# Introduction

This GitLab contains the code used in generating datasets for the training of both CNNs and RL agents to optimize the quadruple and triple splittings performed in the PS.

NOTE: The code has not been generalized fully, so before following instructions below for use make sure you change for example path locations, where you want your simulations to save your results, inside the code itself. See for example the run_eos_tri.py file where a data path is specified.

# Setup

The general idea of the setup is based on generating separate jobs given some inputs, which are then automatically sent forward to the HTCondor batch service for actual computation. To use, one just needs to run either the ```submit_eos_tri.py``` or ```submit_eos_tri_w_csv.py``` with a suitable input, e.g.

```
python3 submit_eos_tri.py -p10 0 -p11 0 -p1p 1 -p20 0 -p21 0 -p2p 1 -v140 1 -v141 1 -v14p 1

or 

python3 submit_eos_tri_w_csv.py # For this, change the .csv files the code takes its inputs from in the actual file.


```

These ```submit_``` files will generate a job submission to be sent to the batch service. The inputs used for submit_eos_tri.py correspond to generate phase/voltage settings based on a linspace command, with the -pX0 command being the lower setting to use, -pX1 the higher end, and -pXp the number of steps to use.

When using the w_csv.py version, the code itself needs to be updated to point to a pre-generated .csv file that will then be used for the simulations.

# Simulation code

The actual simulations being run are defined using BLonD, and can be found in the files ```trisplit_simulation_machine_settings.py``` and ```simulation_doublesplit.py```.
