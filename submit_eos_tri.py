import argparse
import subprocess
import os
import numpy as np
from datetime import datetime

# Example input:  python3 submit_eos_tri.py -p10 0 -p11 0 -p1p 1 -p20 0 -p21 0 -p2p 1 -v140 1 -v141 1 -v14p 1
 
def write_file(phase_14, phase_21, v14_factor):
    s = "executable = activate_eos_tri.sh\n"
    local_iso_ts = datetime.now().astimezone().replace(microsecond=0).isoformat()
    utc_unix_ts = int(round(datetime.timestamp(datetime.utcnow())))
    s += "arguments = -p14 %f -p21 %f -v14 %f -i %s -t %d -cl $(Cluster) -proc $(Process) \n" %\
        (phase_14, phase_21, v14_factor, local_iso_ts, utc_unix_ts)
    #s += "log = outputs/tri/log_p14{}p21{}v14{}.log\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    #s += "error = testing.err\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    #s += "output = outputs/tri/p14{}p21{}v14{}.out\n".format(int(phase_14*100), int(phase_21*100), int(v14_factor*1000))
    s += "request_cpus = 1\n"
    s += "request_disk = 25 MB\n"
    s += "request_memory = 256 MB\n"
    s += "transfer_output_files = eos\n"
    s += "when_to_transfer_output = ON_EXIT\n"
    s += "+MaxDuration = 900\n"
    s += "queue\n"
    return s

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p10', '--start_phase_1', type=float)
    parser.add_argument('-p11', '--stop_phase_1', type=float)
    parser.add_argument('-p1p', '--phase_1_points', type=int)
    parser.add_argument('-p20', '--start_phase_2', type=float)
    parser.add_argument('-p21', '--stop_phase_2', type=float)
    parser.add_argument('-p2p', '--phase_2_points', type=int)
    parser.add_argument('-v140', '--start_v14_factor', type=float)
    parser.add_argument('-v141', '--stop_v14_factor', type=float)
    parser.add_argument('-v14p', '--v14_factor_points', type=int)

    args = parser.parse_args()
    phase_14s = np.linspace(args.start_phase_1, args.stop_phase_1, args.phase_1_points)
    phase_21s = np.linspace(args.start_phase_2, args.stop_phase_2, args.phase_2_points)
    v14_factors = np.linspace(args.start_v14_factor, args.stop_v14_factor, args.v14_factor_points)
 
    for phase_14 in phase_14s:
        for phase_21 in phase_21s:
            for v14_factor in v14_factors:
                f = write_file(phase_14, phase_21, v14_factor)
                sub = "subs/p14{}p21{}v14{}.sub".format(int(phase_14*100),int(phase_21*100), int(v14_factor*1000))
                with open(sub, 'w') as sf:
                    sf.writelines(f)
                subprocess.run(['condor_submit', '%s' % sub])
